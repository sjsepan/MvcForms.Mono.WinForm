﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using Ssepan.Utility.Mono;
using Ssepan.Application.Mono;
using Ssepan.Ui.Mono.WinForm;
using MvcLibrary.Mono;

namespace MvcForms.Mono.WinForm
{
    /// <summary>
    /// Note: this class can subclass the base without type parameters.
    /// </summary>
    public class MVCViewModel :
        FormsViewModel<Image, MVCSettings, MVCModel, MVCView>
    {
        #region Declarations
        #endregion Declarations

        #region Constructors
        public MVCViewModel() { }//Note: not called, but need to be present to compile--SJS

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, Image> actionIconImages,
            FileDialogInfo<Form, DialogResult> settingsFileDialogInfo
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, Image> actionIconImages,
            FileDialogInfo<Form, DialogResult> settingsFileDialogInfo,
            MVCView view
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo, view)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion Constructors

        #region Properties
        #endregion Properties

        #region Methods
        /// <summary>
        /// model specific, not generic
        /// </summary>
        internal void DoSomething()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Doing something...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                ModelController<MVCModel>.Model.SomeBoolean = !ModelController<MVCModel>.Model.SomeBoolean;
                ModelController<MVCModel>.Model.SomeInt += 1;
                ModelController<MVCModel>.Model.SomeString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = !ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
                ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt += 1;
                ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = !ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt += 1;
                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = DateTime.Now.ToString();
                //TODO:implement custom messages
                UpdateStatusBarMessages(null, null, DateTime.Now.ToLongTimeString());

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar("Did something.");
            }
        }

        //override base
        public override void EditPreferences()
        {
            string errorMessage = null;
            FileDialogInfo<Form, DialogResult> fileDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Preferences" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Preferences"],
                    true,
                    33
                );

                //get folder here
                fileDialogInfo = new FileDialogInfo<Form, DialogResult>
                (
                    parent: View,
                    modal: true,
                    title: "Select Folder...",
                    response: DialogResult.None
                );
                
                if (!Dialogs.GetFolderPath(ref fileDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(string.Format("GetFolderPath: {0}", errorMessage));
                }

                if (fileDialogInfo.Response != DialogResult.None)
                {
                    if (fileDialogInfo.Response == DialogResult.OK)
                    {
                        UpdateStatusBarMessages(StatusMessage + fileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                    StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                    return; //if dialog was cancelled
                }

                //override base, which did this
                // if (!Preferences())
                // {
                //     throw new ApplicationException("'Preferences' error");
                // }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("Preferences failed: {0}", ex.Message));
            }
        }

        public /*override*/ void HelpAbout<TAssemblyInfo>()
            where TAssemblyInfo :
            //class,
            AssemblyInfoBase<Form>,
            new()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;
            TAssemblyInfo assemblyInfo = null;
            AboutDialogInfo<Form, DialogResult, Image> aboutDialogInfo = null;
            DialogResult response = DialogResult.None;
            string errorMessage = null;

            try
            {

                StartProgressBar
                (
                    "About" + ACTION_IN_PROGRESS, 
                    null, 
                    _actionIconImages["About"], 
                    true, 
                    33
                );

                //assemblyInfo NOT passed in;specifically, NOT called by override of HelpAbout<TAssemblyInfo>()
                assemblyInfo = new TAssemblyInfo();
                Debug.Assert(this.View != null);

                // use GUI mode About feature
                aboutDialogInfo = 
                    new AboutDialogInfo<Form, DialogResult, Image>
                    (
                        parent : this.View,
                        response : response,
                        modal : true,
                        title : "About " + assemblyInfo.Title,
                        programName : assemblyInfo.Product,
                        version : assemblyInfo.Version,
                        copyright : assemblyInfo.Copyright,
                        comments : assemblyInfo.Description,
                        website : assemblyInfo.Website,
                        //TODO:use logo from loaded resources;this code only works because this lib is run from the context of the calling app
                        logo : null,//TODO:this path is not xplat compatible
                        websiteLabel : assemblyInfo.WebsiteLabel,
                        designers : assemblyInfo.Designers,
                        developers : assemblyInfo.Developers,
                        documenters : assemblyInfo.Documenters,
                        license : assemblyInfo.License
                    );

                if (!Dialogs.ShowAboutDialog(ref aboutDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(errorMessage);
                }
                
                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        /// <summary>
        /// get font name
        /// </summary>
        public void GetFont()
        {
            string errorMessage = null;
            FontDialogInfo<Form, DialogResult, Font, Color> fontDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Font" + ACTION_IN_PROGRESS,
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                //Note:must be set or gets null ref error after dialog closes
                Font fontDescription = null;
                fontDialogInfo = new FontDialogInfo<Form, DialogResult, Font, Color>
                (
                    parent: View,
                    modal: true,
                    title: "Select Font",
                    response: DialogResult.None,
                    fontDescription: fontDescription
                );

                if (!Dialogs.GetFont(ref fontDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(string.Format("GetFolderPath: {0}", errorMessage));
                }

                if (fontDialogInfo.Response != DialogResult.None)
                {
                    if (fontDialogInfo.Response == DialogResult.OK)
                    {
                        UpdateStatusBarMessages(StatusMessage + fontDialogInfo.FontDescription.ToString() + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }

        /// <summary>
        /// get color RGB
        /// </summary>
        public void GetColor()
        {
            string errorMessage = null;
            ColorDialogInfo<Form, DialogResult, Color> colorDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Color" + ACTION_IN_PROGRESS,
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                Color color = Color.FromArgb(0, 0, 0);
                colorDialogInfo = new ColorDialogInfo<Form, DialogResult, Color>
                (
                    parent: View,
                    modal: true,
                    title: "Select Color",
                    response: DialogResult.None,
                    color: color
                );
                
                if (!Dialogs.GetColor(ref colorDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(string.Format("GetFolderPath: {0}", errorMessage));
                }

                if (colorDialogInfo.Response != DialogResult.None)
                {
                    if (colorDialogInfo.Response == DialogResult.OK)
                    {
                        UpdateStatusBarMessages(StatusMessage + colorDialogInfo.Color + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
        }
        #endregion Methods

    }
}
