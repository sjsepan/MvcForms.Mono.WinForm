# readme.md - README for MvcForms.Mono.WinForm

## About

Desktop GUI app demo, on Linux(/Win/Mac/GhostBSD), in C# / Mono / WinForms, using VSCode; requires ssepan.utility.mono, ssepan.io.mono, ssepan.application.mono, ssepan.ui.mono.winform, mvclibrary.mono.

![MvcForms.Mono.WinForm.png](./MvcForms.Mono.WinForm.png?raw=true "Screenshot")

### Notes

I have used the partial-class code-only implementation because...

1) it is closest to the way that Visual Studio seemed to do things, and
2) intellisense on the classes should be available for code-only.

I will be coding MVCView.cs to include a WinForms designer (MVCView.designer.cs) as an example of what I would hope some form designer / editor would consume / produce.
I will also be including an associated .resx file for icons.

### Usage notes

~This application uses keys in app.config to identify a settings file to automatically load, for manual processing and saving. 1) The file name is given, and the format given must be one of two that the app library Ssepan.Application.Core knows how to serialize / deserialize: json, xml. 2) The value used must correspond to the format expected by the SettingsController in Ssepan.Application.Core. It is specified with the SerializeAs property of type SerializationFormat enum. 3) The file must reside in the user's personal directory, which on Linux is /home/username. A sample file can be found in the MvcLibrary.Core project in the Sample folder.
~The automatic loading is not necessary (it is a carry-over from the original console app) and can be disabled by editing the code in InitViewModel() in MvcView.cs to remove the ViewModel.FileOpen() call.

### VSCode

To nest files associated with forms, use the setting 'Features/Explorer/File Nesting: Patterns':
Item="*.cs", Value="${basename}.resx,${basename}.Designer.cs"

### Instructions for downloading/installing Mono

Locate / install 'mono-complete' from your distribution's package manager.

If you are using Visual Basic, then you will likely also want to install ​mono-vbnc​ for dependencies such as ​Microsoft.VisualBasic​.
sudo apt-get install mono-vbnc

### Instructions for downloading/installing Newtonsoft.Json

JSON serialization is used by Ssepan.Application.Mono, but is not available in System.Text[.Json.Serialization]. To get JSON working, install from package manager:
<https://www.codeproject.com/Questions/747284/Json-NET-doesnt-work-with-Mono-under-Debian>
<https://stackoverflow.com/questions/36550910/what-is-the-json-net-mono-assembly-reference>
sudo apt-get install Newtonsoft.Json

### Build / Run from terminal

Build from the same folder as the .csproj, with "xbuild MvcForms.Mono.WinForm.csproj".
If you try to run ('mono MvcForms.Mono.WinForm.exe') from MvcForms.Mono.WinForm project folder, you will get an error "Cannot open assembly 'MvcForms.Mono.WinForm.exe': No such file or directory.". Make sure you run from the bin/Debug sub-folder.

### Issues

~When laying out statusbar, menubar and toolbar, docking of the latter two at the top of the form will be goverened by the order in which they are added to the form's Controls collection. Note that the most recently added will get first shot at the top of the form, so add the menu after the toolbar.
~When runtime errors occur during the use of a dialog, the close event of the application main window seems to be triggered. The quit-confirmation catches this, so you can cancel. This appears to be related to known issues in Mono/Gtk on Linux. One example:
<https://github.com/AvaloniaUI/Avalonia/issues/729>
~Json read/write disabled for now
Installed the Newtonsoft.Json package for ubuntu, but it does not handle the format correctly, so disabled in Ssepan.Application.Mono

### History

0.10:
~add missing propertychangehandler assignment in Program
~add missing DoSwitches call in Program

0.9:
~Fix file-opening from App.Config
~Bring command-line parameter-handling into parity with .Net Core version of apps

0.8:
~initial release

Steve Sepan
<sjsepan@yahoo.com>
6/10/2024
