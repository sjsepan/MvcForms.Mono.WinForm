﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Ssepan.Utility.Mono;
using System.ComponentModel;
using Ssepan.Application.Mono;

namespace MvcForms.Mono.WinForm
{
	static class Program 
    {
        #region Declarations
        public const string APP_NAME = "MvcView";
        #endregion Declarations
        
        #region INotifyPropertyChanged
        public static event PropertyChangedEventHandler PropertyChanged;

        public static void OnPropertyChanged(string propertyName)
        {
            try
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(null, new PropertyChangedEventArgs(propertyName));
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion INotifyPropertyChanged

        #region PropertyChangedEventHandlerDelegate
        /// <summary>
        /// Note: property changes update UI manually.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void PropertyChangedEventHandlerDelegate
        (
            object sender,
            PropertyChangedEventArgs e
        )
        {
            try
            {
                if (e.PropertyName == "Filename")
                {
                    // ConsoleApplication.DefaultOutputDelegate(string.Format("p Filename:{0}", Filename));
                }
                else if (e.PropertyName == "Directory")
                {
                    // ConsoleApplication.DefaultOutputDelegate(string.Format("p Directory:{0}", Directory));
                }
                else if (e.PropertyName == "Format")
                {
                    // ConsoleApplication.DefaultOutputDelegate(string.Format("p Format:{0}", Format));
                }
                else
                {
                    ConsoleApplication.DefaultOutputDelegate(string.Format("{0}", e.PropertyName));
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion PropertyChangedEventHandlerDelegate

        #region Properties
        private static string _Filename = default(string);
        public static string Filename
        {
            get { return _Filename; }
            set
            {
                _Filename = value;
                OnPropertyChanged(nameof(Filename));
            }
        }

        private static string _Directory;
        public static string Directory
        {
            get { return _Directory; }
            set
            {
                _Directory = value;
                OnPropertyChanged(nameof(Directory));
            }
        }

        private static string _Format; //json or xml
        public static string Format
        {
            get { return _Format; }
            set
            {
                _Format = value;
                OnPropertyChanged(nameof(Format));
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args"></param>
        /// <returns>int</returns>
        [STAThread]
		public static int Main(string[] args)
        {
            //default to fail code
            int returnValue = -1;

            try
            {

                //subscribe to notifications
                PropertyChanged += PropertyChangedEventHandlerDelegate;

                //load, parse, run switches
                DoSwitches(args);
                // Console.WriteLine("Main:Filename=" + Filename);

                MVCView mvcView = new MVCView();
                Application.Run(mvcView);

                //return success code
                returnValue = 0;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            finally
            {
            }
            return returnValue;
        }


        #region FormAppBase
        /// <summary>
        /// Note: switches are processed before Model or Settings are accessed.
        /// </summary>
        /// <param name="args"></param>
        static void DoSwitches(string[] args)
        {
            //define supported switches
            // -t -f:"filename" -h
            ConsoleApplication.DoCommandLineSwitches
            (
                args,
                new CommandLineSwitch[]  
                { 
                    new CommandLineSwitch("p", "p filepath; overrides app.config", true, P),
                    new CommandLineSwitch("f", "f format (json/xml); overrides app.config", true, F),
                    // new CommandLineSwitch("a", "a; display About info", true, A)//,
                    //new CommandLineSwitch("H", "H invokes the Help command.", false, ConsoleApplication.Help)//may already be loaded
                }
            );
        }
        #endregion FormAppBase

        #region CommandLineSwitch Action Delegates
        /// <summary>
        /// Name and / or Directory of settings file.
        /// Instance of an action conforming to delegate Action<T>, where T is string.
        /// </summary>
        /// <param name="value">string. Path components containing directory and / or filename</param>
        /// <param name="outputDelegate"></param>
        static void P(string value, Action<string> outputDelegate)
        {
            try
            {
                //validate settings file path
                string directory = Path.GetDirectoryName(value);
                if (directory.StartsWith("~"))
                {
                    directory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), directory.Substring(2));//skip 1st character and also 2nd in case it is a path separator
                }
                if (!string.IsNullOrEmpty(directory))
                {
                    //will be null if not passed or not valid
                    Directory = directory;
                }

                //validate settings file name
                string filename = Path.GetFileName(value);
                if (!string.IsNullOrEmpty(filename))
                {
                    //will be null if not passed
                    Filename = filename;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        /// <summary>
        /// Format of settings file
        /// Instance of an action conforming to delegate Action<T>, where T is string.
        /// </summary>
        /// <param name="value">string. Format name: xml|json</param>
        /// <param name="outputDelegate"></param>
        static void F(string value, Action<string> outputDelegate)
        {
            try
            {
                //validate format
                if (!string.IsNullOrEmpty(value))
                {
                    switch (value)
                    {
                        case "xml":
                        case "json":
                            Format = value;
                            break;
                        default:
                            Format = default(string);
                            break;
                    }
                    // Format = value switch
                    // {
                    //     "xml" or "json" => value,
                    //     _ => default,
                    // };
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
//         /// <summary>
//         /// Validate and set selected settings.
//         /// Instance of an action conforming to delegate Action<T>, where T is string.
//         /// </summary>
//         /// <param name="value"></param>
//         /// <param name="outputDelegate"></param>
//         static void f(string value, Action<string> outputDelegate)
//         {
//             try
//             {
// #if debug
//                 outputDelegate(string.Format("s{0}\t{1}", ConsoleApplication.CommandLineSwitchValueSeparator, value));
// #endif

//                 //validate settings file path
//                 if (!System.IO.File.Exists(value))
//                 {
//                     throw new ArgumentException(string.Format("Invalid settings file path: '{0}'", value));
//                 }
//                 Filename = value;
//             }
//             catch (Exception ex)
//             {
//                 Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
//                 throw;
//             }
//         }
        #endregion CommandLineSwitch Action Delegates
        #endregion Methods
    }
}
