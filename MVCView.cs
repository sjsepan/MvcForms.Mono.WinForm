﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ssepan.Utility.Mono;
using Ssepan.Io.Mono;
using Ssepan.Application.Mono;
using Ssepan.Ui.Mono.WinForm;
using MvcLibrary.Mono;

namespace MvcForms.Mono.WinForm
{
    public partial class MVCView : 
        Form,
        INotifyPropertyChanged
    {
        #region Declarations
        protected bool disposed;
        // private bool _ValueChangedProgrammatically;

        internal Dictionary<string, Image> imageResources = null;

        //cancellation hook
        // System.Action cancelDelegate = null;
        

        protected MVCViewModel ViewModel = default(MVCViewModel);

        #endregion Declarations

        #region Constructors    
        public MVCView() 
        {
            try
            {
                InitializeComponent();

                StatusBarStatusMessage.Text = "";
                StatusBarErrorMessage.Text = "";

                ////(re)define default output delegate
                //ConsoleApplication.defaultOutputDelegate = ConsoleApplication.messageBoxWrapperOutputDelegate;

                //subscribe to view's notifications
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged += PropertyChangedEventHandlerDelegate;
                }

                InitViewModel();

                BindSizeAndLocation();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

        }
        #endregion Constructors

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            try
            {
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion INotifyPropertyChanged

        #region PropertyChangedEventHandlerDelegates
        /// <summary>
        /// Note: model property changes update UI manually.
        /// Note: handle settings property changes manually.
        /// Note: because settings properties are a subset of the model 
        ///  (every settings property should be in the model, 
        ///  but not every model property is persisted to settings)
        ///  it is decided that for now the settings handler will 
        ///  invoke the model handler as well.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PropertyChangedEventHandlerDelegate(object sender, PropertyChangedEventArgs e)
        {
            try
            {
                #region Model
                if (e.PropertyName == "IsChanged")
                {
                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", e.PropertyName));
                    ApplySettings();
                }
                //Status Bar
                else if (e.PropertyName == "ActionIconIsVisible")
                {
                    StatusBarActionIcon.Visible = (ViewModel.ActionIconIsVisible);
                }
                else if (e.PropertyName == "ActionIconImage")
                {
                    StatusBarActionIcon.Image = (ViewModel != null ? ViewModel.ActionIconImage : null);
                }
                else if (e.PropertyName == "StatusMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    StatusBarStatusMessage.Text = (ViewModel != null ? ViewModel.StatusMessage : (string)null);
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", StatusMessage));
                }
                else if (e.PropertyName == "ErrorMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    StatusBarErrorMessage.Text = (ViewModel != null ? ViewModel.ErrorMessage : (string)null);
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "CustomMessage")
                {
                    //replace default action by setting control property
                    //StatusBarCustomMessage.Text = ViewModel.CustomMessage;
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "ErrorMessageToolTipText")
                {
                    StatusBarErrorMessage.ToolTipText = ViewModel.ErrorMessageToolTipText;
                }
                else if (e.PropertyName == "ProgressBarValue")
                {
                    StatusBarProgressBar.Value = ViewModel.ProgressBarValue;
                }
                else if (e.PropertyName == "ProgressBarMaximum")
                {
                    StatusBarProgressBar.Maximum = ViewModel.ProgressBarMaximum;
                }
                else if (e.PropertyName == "ProgressBarMinimum")
                {
                    StatusBarProgressBar.Minimum = ViewModel.ProgressBarMinimum;
                }
                else if (e.PropertyName == "ProgressBarStep")
                {
                    // StatusBarProgressBar.PulseStep = ViewModel.ProgressBarStep;
                }
                else if (e.PropertyName == "ProgressBarIsMarquee")
                {
                    StatusBarProgressBar.Style = (ViewModel.ProgressBarIsMarquee ? ProgressBarStyle.Marquee : ProgressBarStyle.Blocks);//true;
                }
                else if (e.PropertyName == "ProgressBarIsVisible")
                {
                    StatusBarProgressBar.Visible = (ViewModel.ProgressBarIsVisible);
                }
                else if (e.PropertyName == "DirtyIconIsVisible")
                {
                    StatusBarDirtyIcon.Visible = (ViewModel.DirtyIconIsVisible);
                }
                else if (e.PropertyName == "DirtyIconImage")
                {
                    StatusBarDirtyIcon.Image = ViewModel.DirtyIconImage;
                }
                //use if properties cannot be databound
                else if (e.PropertyName == "SomeInt")
                {
                   txtSomeInt.Text = ModelController<MVCModel>.Model.SomeInt.ToString();
                }
                else if (e.PropertyName == "SomeBoolean")
                {
                   chkSomeBoolean.Checked = ModelController<MVCModel>.Model.SomeBoolean;
                }
                else if (e.PropertyName == "SomeString")
                {
                   txtSomeString.Text = ModelController<MVCModel>.Model.SomeString;
                }
                else if (e.PropertyName == "StillAnotherInt")
                {
                   txtStillAnotherInt.Text = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt.ToString();
                }
                else if (e.PropertyName == "StillAnotherBoolean")
                {
                   chkStillAnotherBoolean.Checked = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
                }
                else if (e.PropertyName == "StillAnotherString")
                {
                   txtStillAnotherString.Text = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString;
                }
                else if (e.PropertyName == "SomeOtherInt")
                {
                   txtSomeOtherInt.Text = ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt.ToString();
                }
                else if (e.PropertyName == "SomeOtherBoolean")
                {
                   chkSomeOtherBoolean.Checked = ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
                }
                else if (e.PropertyName == "SomeOtherString")
                {
                   txtSomeOtherString.Text = ModelController<MVCModel>.Model.SomeComponent.SomeOtherString;
                }
                //else if (e.PropertyName == "SomeComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(string.Format("SomeComponent: {0},{1},{2}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt, ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean, ModelController<MVCModel>.Model.SomeComponent.SomeOtherString));
                //}
                //else if (e.PropertyName == "StillAnotherComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(string.Format("StillAnotherComponent: {0},{1},{2}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString));
                //}
                else
                {
                    #if DEBUG_MODEL_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
                #endregion Model

                #region Settings
                if (e.PropertyName == "Dirty")
                {
                    //apply settings that don't have databindings
                    ViewModel.DirtyIconIsVisible = (SettingsController<MVCSettings>.Settings.Dirty);
                }
                else
                {
                    #if DEBUG_SETTINGS_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
                #endregion Settings
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion PropertyChangedEventHandlerDelegates

        #region Properties
        private string _ViewName = Program.APP_NAME;
        public string ViewName
        {
            get { return _ViewName; }
            set
            {
                _ViewName = value;
                OnPropertyChanged("ViewName");
            }
        }
        #endregion Properties

        #region Events

        #region Form Events
        private void View_Load(object sender, EventArgs e)
        {
            try
            {
                ViewModel.StatusMessage = string.Format("{0} starting...", ViewName);

                ViewModel.StatusMessage = string.Format("{0} started.", ViewName);

                //_Run();//only called here in console apps; use button in forms apps
            }
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                ViewModel.StatusMessage = string.Empty;

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        private void View_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool resultDontQuit = false;

            try
            {
                ViewModel.FileExit(ref resultDontQuit);

                ((System.ComponentModel.CancelEventArgs)e).Cancel = resultDontQuit;

                if (!resultDontQuit)
                {
                    //clean up data model here
                    ViewModel.StatusMessage = string.Format("{0} completing...", ViewName);
                    DisposeSettings();
                    ViewModel.StatusMessage = string.Format("{0} completed.", ViewName);

                    ViewModel = null;

                    //Application.Instance.Quit();//this will only re-trigger the Closing event; just allow window to close
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                if (ViewModel != null)
                {
                    ViewModel.ErrorMessage = ex.Message;
                }
            }
        }
        #endregion Form Events

        #region Control Events
        private void txtSomeInt_TextChanged(object sender, EventArgs e)
        {
            int result = 0;

            ModelController<MVCModel>.Model.SomeInt = 
                (int.TryParse(txtSomeInt.Text, out result) ? result : 0);
        }

        private void txtSomeOtherInt_TextChanged(object sender, EventArgs e)
        {
            int result = 0;
            
            ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt = 
                (int.TryParse(txtSomeOtherInt.Text, out result) ? result : 0);
        }

        private void txtStillAnotherInt_TextChanged(object sender, EventArgs e)
        {
            int result = 0;
            
            ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt = 
                (int.TryParse(txtStillAnotherInt.Text, out result) ? result : 0);
        }

        private void txtSomeString_TextChanged(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeString = txtSomeString.Text;
        }

        private void txtSomeOtherString_TextChanged(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = txtSomeOtherString.Text;
        }

        private void txtStillAnotherString_TextChanged(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = txtStillAnotherString.Text;
        }

        private void chkSomeBoolean_CheckChanged(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeBoolean = chkSomeBoolean.Checked;
        }

        private void chkSomeOtherBoolean_CheckChanged(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = chkSomeOtherBoolean.Checked;
        }

        private void chkStillAnotherBoolean_CheckChanged(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = chkStillAnotherBoolean.Checked;
        }
        private void cmdRun_Click(object sender, EventArgs e)
        {
            //do something
            ViewModel.DoSomething();
        }
        private void cmdColor_Click(object sender, EventArgs e)
		{
            ViewModel.GetColor();
        }
        private void cmdFont_Click(object sender, EventArgs e)
		{
            ViewModel.GetFont();
        }
        #endregion Control Events

        #region Menu Events
        private void MenuFileNew_Click(object sender, EventArgs e)
		{
            ViewModel.FileNew();
        }
        private void MenuFileOpen_Click(object sender, EventArgs e)
		{
            ViewModel.FileOpen();
        }
        private void MenuFileSave_Click(object sender, EventArgs e)
		{
            ViewModel.FileSave();
        }
        private void MenuFileSaveAs_Click(object sender, EventArgs e)
		{
            ViewModel.FileSaveAs();
        }
        private void MenuFilePrint_Click(object sender, EventArgs e)
		{
            ViewModel.FilePrint();
        }
        private void MenuFilePrintPreview_Click(object sender, EventArgs e)
		{
            ViewModel.FilePrintPreview();
        }
        private void MenuFileQuit_Click(object sender, EventArgs e)
		{
            this.Close();
        }
        private void MenuEditUndo_Click(object sender, EventArgs e)
		{
            ViewModel.EditUndo();
        }
        private void MenuEditRedo_Click(object sender, EventArgs e)
		{
            ViewModel.EditRedo();
        }
        private void MenuEditSelectAll_Click(object sender, EventArgs e)
		{
            ViewModel.EditSelectAll();
        }
        private void MenuEditCut_Click(object sender, EventArgs e)
		{
            ViewModel.EditCut();
        }
        private void MenuEditCopy_Click(object sender, EventArgs e)
		{
            ViewModel.EditCopy();
        }
        private void MenuEditPaste_Click(object sender, EventArgs e)
		{
            ViewModel.EditPaste();
        }
        private void MenuEditPasteSpecial_Click(object sender, EventArgs e)
		{
            ViewModel.EditPasteSpecial();
        }
        private void MenuEditDelete_Click(object sender, EventArgs e)
		{
            ViewModel.EditDelete();
        }
        private void MenuEditFind_Click(object sender, EventArgs e)
		{
            ViewModel.EditFind();
        }
        private void MenuEditReplace_Click(object sender, EventArgs e)
		{
            ViewModel.EditReplace();
        }
        private void MenuEditRefresh_Click(object sender, EventArgs e)
		{
            ViewModel.EditRefresh();
        }
        private void MenuEditPreferences_Click(object sender, EventArgs e)
		{
            ViewModel.EditPreferences();
        }
        private void MenuEditProperties_Click(object sender, EventArgs e)
		{
            ViewModel.EditProperties();
        }
        private void MenuWindowNewWindow_Click(object sender, EventArgs e)
		{
            ViewModel.WindowNewWindow();
        }
        private void MenuWindowTile_Click(object sender, EventArgs e)
		{
            ViewModel.WindowTile();
        }
        private void MenuWindowCascade_Click(object sender, EventArgs e)
		{
            ViewModel.WindowCascade();
        }
        private void MenuWindowArrangeAll_Click(object sender, EventArgs e)
		{
            ViewModel.WindowArrangeAll();
        }
        private void MenuWindowHide_Click(object sender, EventArgs e)
		{
            ViewModel.WindowHide();
        }
        private void MenuWindowShow_Click(object sender, EventArgs e)
		{
            ViewModel.WindowShow();
        }
        private void MenuHelpContents_Click(object sender, EventArgs e)
		{
            ViewModel.HelpContents();
        }
        private void MenuHelpIndex_Click(object sender, EventArgs e)
		{
            ViewModel.HelpIndex();
        }
        private void MenuHelpOnlineHelp_Click(object sender, EventArgs e)
		{
            ViewModel.HelpOnlineHelp();
        }
        private void MenuHelpLicenceInformation_Click(object sender, EventArgs e)
		{
            ViewModel.HelpLicenceInformation();
        }
        private void MenuHelpCheckForUpdates_Click(object sender, EventArgs e)
		{
            ViewModel.HelpCheckForUpdates();
        }
        private void MenuHelpAbout_Click(object sender, EventArgs e)
        {
            ViewModel.HelpAbout<AssemblyInfo>();
        }
        #endregion Menu Events
        #endregion Events

        #region Methods
        #region FormAppBase
        protected void InitViewModel()
        {
            FileDialogInfo<Form, DialogResult> settingsFileDialogInfo = null;
            
            try
            {
                //tell controller how model should notify view about non-persisted properties AND including model properties that may be part of settings
                ModelController<MVCModel>.DefaultHandler = PropertyChangedEventHandlerDelegate;

                //tell controller how settings should notify view about persisted properties
                SettingsController<MVCSettings>.DefaultHandler = PropertyChangedEventHandlerDelegate;

                InitModelAndSettings();

                //settings used with file dialog interactions
                settingsFileDialogInfo =
                    new FileDialogInfo<Form, DialogResult>
                    (
                        parent: this,
                        modal: true,
                        title: null,
                        response: DialogResult.None,
                        newFilename: SettingsController<MVCSettings>.FILE_NEW,
                        filename: null,
                        extension: MVCSettings.FileTypeExtension,
                        description: MVCSettings.FileTypeDescription,
                        typeName: MVCSettings.FileTypeName,
                        additionalFilters: new string[] 
                        { 
                            //"MvcSettings files (*.mvcsettings)|*.mvcsettings", //default (xml) defined in SettingsBase, overridden in MVCSettings
                            //"JSON files (*.json)|*.json", //Note:need to find JSON support in Mono
                            "XML files (*.xml)|*.xml", 
                            "All files (*.*)|*.*" 
                        },
                        multiselect: false,
                        initialDirectory: default(Environment.SpecialFolder),
                        forceDialog: false,
                        forceNew: false,
                        customInitialDirectory: Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
                    );

                //set dialog caption
                settingsFileDialogInfo.Title = this.Text;

                //class to handle standard behaviors
                ViewModelController<Image, MVCViewModel>.New
                (
                    ViewName,
                    new MVCViewModel
                    (
                        this.PropertyChangedEventHandlerDelegate,
                        new Dictionary<string, Image>() 
                        { //get from Resources; error conversting from Icon to Image
                            // { "App", global::MvcForms.Mono.WinForm.Properties.Resources.App },
                            { "New", global::MvcForms.Mono.WinForm.Properties.Resources.New },
                            { "Open", global::MvcForms.Mono.WinForm.Properties.Resources.Open },
                            { "Save", global::MvcForms.Mono.WinForm.Properties.Resources.Save },
                            { "Print", global::MvcForms.Mono.WinForm.Properties.Resources.Print },
                            { "Undo", global::MvcForms.Mono.WinForm.Properties.Resources.Undo },
                            { "Redo", global::MvcForms.Mono.WinForm.Properties.Resources.Redo },
                            { "Cut", global::MvcForms.Mono.WinForm.Properties.Resources.Cut },
                            { "Copy", global::MvcForms.Mono.WinForm.Properties.Resources.Copy },
                            { "Paste", global::MvcForms.Mono.WinForm.Properties.Resources.Paste },
                            { "Delete", global::MvcForms.Mono.WinForm.Properties.Resources.Delete },
                            { "Find", global::MvcForms.Mono.WinForm.Properties.Resources.Find },
                            { "Replace", global::MvcForms.Mono.WinForm.Properties.Resources.Replace },
                            { "Refresh", global::MvcForms.Mono.WinForm.Properties.Resources.Reload },
                            { "Preferences", global::MvcForms.Mono.WinForm.Properties.Resources.Preferences },
                            { "Properties", global::MvcForms.Mono.WinForm.Properties.Resources.Properties },
                            { "Contents", global::MvcForms.Mono.WinForm.Properties.Resources.Contents },
                            { "About", global::MvcForms.Mono.WinForm.Properties.Resources.About },
                            { "Network", global::MvcForms.Mono.WinForm.Properties.Resources.Network }
                        },
                        settingsFileDialogInfo,
                        this
                    )
                );

                //select a viewmodel by view name
                ViewModel = ViewModelController<Image, MVCViewModel>.ViewModel[ViewName];

                BindFormUi();

                //Init config parameters
                if (!LoadParameters())
                {
                    throw new Exception(string.Format("Unable to load config file parameter(s)."));
                }

                //DEBUG:filename coming in is being converted/passed as DOS 8.3 format equivalent
                //Load
                if ((SettingsController<MVCSettings>.FilePath == null) || (SettingsController<MVCSettings>.Filename.StartsWith(SettingsController<MVCSettings>.FILE_NEW)))
                {
                    //NEW
                    ViewModel.FileNew();
                }
                else
                {
                    //OPEN
                    ViewModel.FileOpen(false);
                }

#if debug
            //debug view
            menuEditProperties_Click(sender, e);
#endif

                //Display dirty state
                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                if (ViewModel != null)
                {
                    ViewModel.ErrorMessage = ex.Message;
                }
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        protected void InitModelAndSettings()
        {
            //create Settings before first use by Model
            if (SettingsController<MVCSettings>.Settings == null)
            {
                SettingsController<MVCSettings>.New();
            }
            //Model properties rely on Settings, so don't call Refresh before this is run.
            if (ModelController<MVCModel>.Model == null)
            {
                ModelController<MVCModel>.New();
            }
        }

        protected void DisposeSettings()
        {
            MessageDialogInfo<Form, DialogResult, object, MessageBoxIcon, MessageBoxButtons> questionMessageDialogInfo = null;
            string StatusBarErrorMessage = null;

            //save user and application settings 
            // MvcForms.Mono.WinForm.Properties.Settings.Default.Save();//BUG:seems to be ignored

            if (SettingsController<MVCSettings>.Settings.Dirty)
            {
                //prompt before saving
                questionMessageDialogInfo = new MessageDialogInfo<Form, DialogResult, object, MessageBoxIcon, MessageBoxButtons>
                (
                    this,
                    true,
                    this.Text,
                    null,
                    MessageBoxIcon.Question,
                    MessageBoxButtons.YesNo,
                    "Save changes?",
                    DialogResult.None
                );
                if (!Dialogs.ShowMessageDialog(ref questionMessageDialogInfo, ref StatusBarErrorMessage))
                {
                    throw new ApplicationException(StatusBarErrorMessage);
                }

                // DialogResult dialogResult = MessageBox.Show("Save changes?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                switch (questionMessageDialogInfo.Response)
                {
                    case DialogResult.Yes:
                        {
                            //SAVE
                            ViewModel.FileSave();

                            break;
                        }
                    case DialogResult.No:
                        {
                            break;
                        }
                    default:
                        {
                            throw new InvalidEnumArgumentException();
                        }
                }
            }

            //unsubscribe from model notifications
            ModelController<MVCModel>.Model.PropertyChanged -= PropertyChangedEventHandlerDelegate;
        }

        // protected void _Run()
        // {
        //     //MessageBox.Show("running", "MVC", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        // }
        #endregion FormAppBase

        #region Utility
        /// <summary>
        /// Bind static Model controls to Model Controller
        /// </summary>
        private void BindFormUi()
        {
            try
            {
                //Form

                //Controls
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Bind Model controls to Model Controller
        /// Note: databinding not used in Mono implementation, but leave this in place 
        ///  in case you want to do this
        /// </summary>
        private void BindModelUi()
        {
            try
            {
                BindField<TextBox, MVCModel>(txtSomeInt, ModelController<MVCModel>.Model, "SomeInt");
                BindField<TextBox, MVCModel>(txtSomeString, ModelController<MVCModel>.Model, "SomeString");
                BindField<CheckBox, MVCModel>(chkSomeBoolean, ModelController<MVCModel>.Model, "SomeBoolean", "Checked");

                BindField<TextBox, MVCModel>(txtSomeOtherInt, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherInt");
                BindField<TextBox, MVCModel>(txtSomeOtherString, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherString");
                BindField<CheckBox, MVCModel>(chkSomeOtherBoolean, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherBoolean", "Checked");
                
                BindField<TextBox, MVCModel>(txtStillAnotherInt, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherInt");
                BindField<TextBox, MVCModel>(txtStillAnotherString, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherString");
                BindField<CheckBox, MVCModel>(chkStillAnotherBoolean, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherBoolean", "Checked");
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// Note: databinding not use in Mono implementation, but leave this in place 
        ///  in case you want to do this
        private void BindField<TControl, TModel>
        (
            TControl fieldControl,
            TModel model,
            string modelPropertyName,
            string controlPropertyName = "Text",
            bool formattingEnabled = false,
            //DataSourceUpdateMode dataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged,
            bool reBind = true
        )
            where TControl : Control
        {
            try
            {
                //TODO: .RemoveSignalHandler ?
                //fieldControl.DataBindings.Clear();
                if (reBind)
                {
                    //TODO:.AddSignalHandler ?
                    //fieldControl.DataBindings.Add(controlPropertyName, model, modelPropertyName, formattingEnabled, dataSourceUpdateMode);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        /// <summary>
        /// Apply Settings to viewer.
        /// </summary>
        private void  ApplySettings()
        {
            try
            {
                // _ValueChangedProgrammatically = true;

                //apply settings that have databindings
                BindModelUi();

                //apply settings that shouldn't use databindings

                //apply settings that can't use databindings
                Text = System.IO.Path.GetFileName(SettingsController<MVCSettings>.Filename) + " - " + ViewName;

                //apply settings that don't have databindings
                ViewModel.DirtyIconIsVisible = (SettingsController<MVCSettings>.Settings.Dirty);
                //update remaining non-bound controls: textboxes, checkboxes
                //Note: ModelController<MVCModel>.Model.Refresh() will cause SO here
                ModelController<MVCModel>.Model.Refresh("SomeInt");
                ModelController<MVCModel>.Model.Refresh("SomeBoolean");
                ModelController<MVCModel>.Model.Refresh("SomeString");
                ModelController<MVCModel>.Model.Refresh("SomeOtherInt");
                ModelController<MVCModel>.Model.Refresh("SomeOtherBoolean");
                ModelController<MVCModel>.Model.Refresh("SomeOtherString");
                ModelController<MVCModel>.Model.Refresh("StillAnotherInt");
                ModelController<MVCModel>.Model.Refresh("StillAnotherBoolean");
                ModelController<MVCModel>.Model.Refresh("StillAnotherString");

                // _ValueChangedProgrammatically = false;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Set function button and menu to enable value, and cancel button to opposite.
        /// For now, do only disabling here and leave enabling based on biz logic 
        ///  to be triggered by refresh?
        /// Note: another feature that is not used, but could be
        /// </summary>
        /// <param name="functionButton"></param>
        /// <param name="functionMenu"></param>
        /// <param name="cancelButton"></param>
        /// <param name="enable"></param>
        private void SetFunctionControlsEnable
        (
            Button functionButton,
            Button functionToolbarButton,
            MenuItem functionMenu,
            Button cancelButton,
            bool enable
        )
        {
            try
            {
                //stand-alone button
                if (functionButton != null)
                {
                    functionButton.Enabled = enable;
                }

                //toolbar button
                if (functionToolbarButton != null)
                {
                    functionToolbarButton.Enabled = enable;
                }

                //menu item
                if (functionMenu != null)
                {
                    functionMenu.Enabled = enable;
                }

                //stand-alone cancel button
                if (cancelButton != null)
                {
                    cancelButton.Enabled = !enable;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        /// <summary>
        /// Invoke any delegate that has been registered 
        ///  to cancel a long-running background process.
        /// Note: another feature that is not used, but could be
        /// </summary>
        // private void InvokeActionCancel()
        // {
        //     try
        //     {
        //         //execute cancellation hook
        //         if (cancelDelegate != null)
        //         {
        //             cancelDelegate();
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Load from app config; override with command line if present
        /// </summary>
        /// <returns></returns>
        private bool LoadParameters()
        {
            bool returnValue = default(bool);

            try
            {
                // First, get configured values

                //get filename from App.config
                if (!Configuration.ReadString("SettingsFilename", out string _settingsFilename))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsFilename: {0}", "SettingsFilename"));
                }
                if ((_settingsFilename == null) || (_settingsFilename == SettingsController<MVCSettings>.FILE_NEW))
                {
                    throw new ApplicationException(string.Format("Settings filename not set: '{0}'.\nCheck SettingsFilename in App.config file.", _settingsFilename));
                }
                //use with the supplied path
                SettingsController<MVCSettings>.Filename = _settingsFilename;

                //identify directory to specified, or use default
                if (System.IO.Path.GetDirectoryName(_settingsFilename)?.Length == 0)
                {
                    //supply default path if missing
                    SettingsController<MVCSettings>.Pathname = Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator();
                }

                //get serialization format from App.config
                if (!Configuration.ReadString("SettingsSerializeAs", out string _settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsSerializeAs: {0}", "SettingsSerializeAs"));
                }
                if (string.IsNullOrEmpty(_settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Settings serialization format not set: '{0}'.\nCheck SettingsSerializeAs in App.config file.", _settingsSerializeAs));
                }
                //use with the filename
                SettingsBase.SerializeAs = SettingsBase.ToSerializationFormat(_settingsSerializeAs);

                //Second, override with passed values

                if ((Program.Filename != default(string)) && (Program.Filename != SettingsController<MVCSettings>.FILE_NEW))
                {
                    //got filename from command line
                    SettingsController<MVCSettings>.Filename = Program.Filename;
                }
                if (Program.Directory != default(string))
                {
                    //get default directory from command line
                    SettingsController<MVCSettings>.Pathname = Program.Directory.WithTrailingSeparator();
                }
                if (Program.Format != default(string))
                {
                    // get default format from command line
                    switch (Program.Format)
                    {
                        case "xml":
                            SettingsBase.SerializeAs = SettingsBase.SerializationFormat.Xml;
                            break;
                        case "json":
                            SettingsBase.SerializeAs = SettingsBase.SerializationFormat.Json;
                            break;
                        default:
                            throw new ApplicationException(string.Format("invalid Serialization Format returned from App.Config: {0}", Program.Format));
                            // break;
                    }
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
            return returnValue;
        }

        private void BindSizeAndLocation()
        {

            //Note:Size must be done after InitializeComponent(); do Location this way as well.--SJS
            // this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::MVCForms.Properties.Settings.Default, "Location", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            // this.DataBindings.Add(new System.Windows.Forms.Binding("ClientSize", global::MVCForms.Properties.Settings.Default, "Size", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            // Console.WriteLine("Width="+global::MvcForms.Mono.WinForm.Properties.Settings.Default.Size.Width);
            // Console.WriteLine("Height="+global::MvcForms.Mono.WinForm.Properties.Settings.Default.Size.Height);
            // this.Size = //BUG:seems to be ignored
            //     new Size
            //     (
            //         global::MvcForms.Mono.WinForm.Properties.Settings.Default.Size.Width,
            //         global::MvcForms.Mono.WinForm.Properties.Settings.Default.Size.Height
            //     );
            // this.Location = 
            //     new Point
            //     (
            //         global::MvcForms.Mono.WinForm.Properties.Settings.Default.Location.X,
            //         global::MvcForms.Mono.WinForm.Properties.Settings.Default.Location.Y
            //     );
        }
        #endregion Utility
        #endregion Methods

        #region Actions
        private async Task DoSomething()
        {
            for (int i = 0; i < 3; i++)
            {
                //StatusBarProgressBar.Pulse();
                //DoEvents;
                await Task.Delay(1000); 
            }
        }

        #endregion Actions

        #region Utility
        private void StartProgressBar()
        {
            StatusBarProgressBar.Value = 33;
            StatusBarProgressBar.Style = ProgressBarStyle.Marquee;//true;
            StatusBarProgressBar.Visible = true;
            //DoEvents;
        }

        private void StopProgressBar()
        {
            //DoEvents;
            StatusBarProgressBar.Visible = false;
        }

        private void StartActionIcon(Image resourceItem/*string resourceItemId*/)
        {
            StatusBarActionIcon.Image = (resourceItem == null ? null : resourceItem);//global::MvcForms.Mono.WinForm.Properties.Resources.New; //"New", etc.
            StatusBarActionIcon.Visible = true;
        }

        private void StopActionIcon()
        {
            StatusBarActionIcon.Visible = false;
            StatusBarActionIcon.Image = global::MvcForms.Mono.WinForm.Properties.Resources.New; 
        }

        #endregion Utility


    }
}
